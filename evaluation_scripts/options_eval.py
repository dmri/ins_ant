import argparse

str2bool = lambda arg: arg.lower() in ("yes", "true", "t", "1")

parser = argparse.ArgumentParser(description="Evaluate a model for surgical instrument anticipation.")
parser.register('type', 'bool', str2bool)

parser.add_argument('--sample_path', type=str, default='../output/experiments/20200821-1203_horizon5_anticipationTest/results/')
parser.add_argument('--out_dir', type=str, default='../output/plots/')
parser.add_argument('--epoch', type=int, default=100)
parser.add_argument('--horizon', type=int, default=5)
parser.add_argument('--uncertainty_type', type=str, default='aleatoric_cls', help='options: (epistemic_reg | epistemic_cls | aleatoric_cls | entropy_cls)')
parser.add_argument('--save', type=bool, default=False)