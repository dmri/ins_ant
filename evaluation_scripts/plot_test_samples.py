import numpy as np
import matplotlib.pyplot as plt
import os
import util_eval as util
from options_eval import parser

opts = parser.parse_args()

horizon = opts.horizon

if opts.save and not os.path.exists(opts.out_dir):
	os.mkdir(opts.out_dir)

for f in sorted(os.listdir(opts.sample_path)):

	if ('sample_epoch_{}_'.format(opts.epoch) in f) and ('pred_reg' in f):
	
		path_pred_reg = os.path.join(opts.sample_path,f)
		path_gt_reg = path_pred_reg.replace('pred_reg','gt_reg')
		path_pred_cls = path_pred_reg.replace('pred_reg','pred_cls')
		path_gt_cls = path_pred_reg.replace('pred_reg','gt_cls')

		prediction_reg = np.load(path_pred_reg)
		target_reg = np.load(path_gt_reg)
		prediction_cls = np.load(path_pred_cls)

		plt.rcParams["figure.figsize"] = (20,10)
		fig, axes = plt.subplots(target_reg.shape[0])
		fig.suptitle(f)

		for i,(ax,pred,gt,pred_cls) in enumerate(zip(axes, prediction_reg, target_reg, prediction_cls)):
			
			x = np.arange(len(gt),dtype=np.float)

			mean = pred.mean(axis=1)

			uncertainties = {
				'epistemic_reg': util.epistemic_reg(pred, dim_samples=1),
				'epistemic_cls': util.epistemic_cls(pred_cls, dim_samples=2, dim_classes=1),
				'aleatoric_cls': util.aleatoric_cls(pred_cls, dim_samples=2, dim_classes=1),
				'entropy_cls': util.entropy_cls(pred_cls, dim_samples=2, dim_classes=1)
			}

			uncertainty = uncertainties[opts.uncertainty_type]
			uncert_name = util.uncert_names[opts.uncertainty_type]
		
			ax.fill_between(x, np.zeros_like(x), np.full_like(x,horizon), where=(gt > 0) & (gt < horizon), facecolor='gray', alpha=.2)
			ax.plot(x, gt, c='black',label = 'Ground truth'	)
			ax.fill_between(x, mean-2*uncertainty, mean+2*uncertainty, facecolor=util.colors[i], alpha=.5, label=uncert_name)
			ax.plot(x, mean, c=util.colors[i], label='Prediction')
			
			ax.set_xlabel('Time [sec.]',size=15)
			ax.set_ylabel('{}\n[min.]'.format(util.instruments[i]),size=15)

			ax.set_yticks([0,horizon/2,horizon])
			ax.set_yticklabels([0,horizon/2,'>'+str(horizon)],size=12)
			ax.set_ylim(-.5,horizon+.5)
			
			ax.legend(fontsize=15)

		if not opts.save:
			plt.show()
		else:
			plt.savefig(opts.out_dir + f.split('.')[0] + '.png')
		plt.close()