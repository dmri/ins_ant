import numpy as np
import matplotlib.pyplot as plt
import os
import torch
import argparse

def create_anticipation_gt_onetool(tool_code,horizon):
    anticipation = torch.zeros_like(tool_code).type(torch.FloatTensor)
    tool_count = horizon
    for i in torch.arange(len(tool_code)-1,-1,-1):
        if tool_code[i]:
            tool_count = 0
        else:
            tool_count = min(1, tool_count + 1/(60*horizon))
        anticipation[i] = tool_count
    return anticipation

def create_anticipation_gt(tools,horizon):
    return torch.stack([create_anticipation_gt_onetool(tool_code,horizon) for tool_code in tools])

def tools_to_target(tools,horizon):

	tools = torch.LongTensor(tools).permute(1,0)
	target = create_anticipation_gt(tools,horizon)
	return target.numpy()

def normalize_op(op,length):
	
	return np.array([op[int((k/length)*len(op))] for k in range(length)])

def get_tool_presence_from_hist(hist):
	return [np.where(hist>threshold,1,0) for threshold in range(1,30)]

def get_prediction(y_init,t,horizon,mean_len,oracle):
	if oracle:
		y = normalize_op(y_init,t.shape[1])
		y = tools_to_target(y,horizon)
	else:
		y = normalize_op(y_init,mean_len)
		y = tools_to_target(y,horizon)
		
		if t.shape[1] <= y.shape[1]:
			y = y[:,:t.shape[1]]
		else:
			y_tmp = y
			y = np.ones_like(t)
			y[:,:y_tmp.shape[1]] = y_tmp
	return y

def compute_histogram(path,IDs,bins=1000):

	ops = []
	for ID in IDs:
		op = np.loadtxt(path + '/video{}-tool.txt'.format(ID), dtype=np.string_, delimiter="\t")[1:,[2, 4, 5, 6, 7]].astype(np.float64)
		ops.append(op)

	num_class = ops[0].shape[1]
	histogram = np.zeros([bins, num_class], dtype=np.float64)

	for op in ops:
		length = len(op)
		for k in range(bins):
			index = int((k/bins)*length)
			histogram[k,:] += op[index]
	return histogram

def load_target(path,IDs,horizon):

	ops = []
	for ID in IDs:
		op = np.loadtxt(path + '/video{}-tool.txt'.format(ID), dtype=np.string_, delimiter="\t")[1:,[2, 4, 5, 6, 7]].astype(np.float64)
		ops.append(tools_to_target(op,horizon))
	return ops

if __name__ == '__main__':
	
	fold_1 = ['02','04','06','12','24','29','34','37','38','39','44','58','60','61','64','66','75','78','79','80']
	fold_2 = ['01','03','05','09','13','16','18','21','22','25','31','36','45','46','48','50','62','71','72','73']
	fold_3 = ['10','15','17','20','32','41','42','43','47','49','51','52','53','55','56','69','70','74','76','77']
	fold_4 = ['07','08','11','14','19','23','26','27','28','30','33','35','40','54','57','59','63','65','67','68']

	instruments = ['bipolar','scissors','clipper','irrigator','specimen bag']

	parser = argparse.ArgumentParser(description="Evaluate a model for surgical instrument anticipation.")
	parser.add_argument('--horizon', type=int, default=3)
	parser.add_argument('--baseline', type=str, default='mean', help='mean or oracle')
	parser.add_argument('--mode', type=str, default='train', help='train or test')
	parser.add_argument('--annotation_folder', type=str, default='../data/annotations/tool_annotations')
	opts = parser.parse_args()

	if opts.baseline not in ['mean','oracle']:
		raise NotImplementedError('<baseline> must be "mean" or "oracle". Got "{}"'.format(opts.baseline))
	if opts.mode not in ['train','test']:
		raise NotImplementedError('<mode> must be "train" or "test". Got "{}"'.format(opts.mode))

	horizon = opts.horizon
	oracle = (opts.baseline == 'oracle')
	train_mode = (opts.mode == 'train')
	path = opts.annotation_folder

	### pre-learned thresholds
	### the indices 0-29 correspond to bin count thresholds of 1-30
	### so threshold = index + 1
	###
	### these thresholds are only used in test mode
	### in train mode, thresholds are learned from the training data
	### since the learning process is deterministic, the same thresholds as below will be computed
	### however, note that the lists below contain the indices and the script prints thresholds (where threshold = index +1)
	
	# length known (OracleHist)
	indices_oracle = {
		2: [12,5,7,12,21], # horizon 2
		3: [12,5,7,12,21], # horizon 3
		5: [12,5,8,12,19], # horizon 5
		7: [13,5,8,14,19]  # horizon 7
	}
	# length unknown (MeanHist)
	indices_mean = {
		2: [13,5,11,16,26],
		3: [13,5,11,16,26],
		5: [13,5,11,16,24],
		7: [13,5,11,16,26]
	}

	train_IDs = fold_1 + fold_2 + fold_3
	test_IDs = fold_4

	train_target = load_target(path,train_IDs,horizon=horizon)
	test_target = load_target(path,test_IDs,horizon=horizon)

	mean_len = int(np.mean([t.shape[1] for t in train_target]))

	hist = compute_histogram(path,train_IDs)
	tool_predictions = get_tool_presence_from_hist(hist)

	if train_mode:

		print('TRAINING')
		print('oracle: ',oracle)
		print('horizon: ',horizon)
		print('train mode: ',train_mode)

		indices = [0,0,0,0,0]
		best_scores = [horizon,horizon,horizon,horizon,horizon]

		# learn the optimal bin count threshold for each of the 5 instruments
		for i in range(5):

			# <tool_predictions> contains histogram-based predictions for all possible bin count thresholds
			# we iterate over all thresholds and select the one which performs best on the training set w.r.t. wMAE
			print(instruments[i])
			for index,y_init in enumerate(tool_predictions):

				pred, target = [], []
				for t in train_target:
					y = get_prediction(y_init,t,horizon,mean_len,oracle)

					pred.append(y[i])
					target.append(t[i])

				pred = np.concatenate(pred)*horizon
				target = np.concatenate(target)*horizon

				if not np.any((pred > 0) & (pred < horizon)):
					break

				OUTSIDE_HORIZON = (target == horizon)
				INSIDE_HORIZON = (target > 0) & (target < horizon)

				outsideMAE = np.abs(pred[OUTSIDE_HORIZON]-target[OUTSIDE_HORIZON]).mean()
				insideMAE = np.abs(pred[INSIDE_HORIZON]-target[INSIDE_HORIZON]).mean()
				wMAE = (outsideMAE + insideMAE) / 2

				if wMAE < best_scores[i]:
					indices[i] = index
					best_scores[i] = wMAE

				print('Threshold {}: wMAE {:.2f}'.format(index+1,wMAE))
			print('Best threshold for {}: {}'.format(instruments[i],indices[i]+1))

	else:
		if oracle:
			indices = indices_oracle[horizon]
		else:
			indices = indices_mean[horizon]

	print('TEST')
	print('oracle: ',oracle)
	print('horizon: ',horizon)
	print('')

	scores_wMAE, scores_pMAE = [], []
	for i in range(5):

		print(instruments[i])
		y_init = tool_predictions[indices[i]]

		pred, target = [], []
		for t in test_target:
			y = get_prediction(y_init,t,horizon,mean_len,oracle)

			pred.append(y[i])
			target.append(t[i])

		pred = np.concatenate(pred)*horizon
		target = np.concatenate(target)*horizon

		OUTSIDE_HORIZON = (target == horizon)
		INSIDE_HORIZON = (target > 0) & (target < horizon)
		ANTICIPATING = (pred > horizon*.1) & (pred < horizon*.9)

		outsideMAE = np.abs(pred[OUTSIDE_HORIZON]-target[OUTSIDE_HORIZON]).mean()
		insideMAE = np.abs(pred[INSIDE_HORIZON]-target[INSIDE_HORIZON]).mean()
		wMAE = (outsideMAE + insideMAE) / 2

		pMAE = np.abs(pred[ANTICIPATING]-target[ANTICIPATING]).mean()

		scores_wMAE.append(wMAE)
		scores_pMAE.append(pMAE)

		print('wMAE: {:.2f}, pMAE: {:.2f}'.format(wMAE,pMAE))

	print('mean')
	print('wMAE: {:.2f}, pMAE: {:.2f}'.format(np.mean(scores_wMAE),np.mean(scores_pMAE)))